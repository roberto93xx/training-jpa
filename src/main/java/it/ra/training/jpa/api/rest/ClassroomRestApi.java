package it.ra.training.jpa.api.rest;

import java.util.List;
import java.util.logging.Logger;

import javax.ejb.EJB;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import it.ra.training.jpa.api.rest.converter.ClassroomEntityRestConverter;
import it.ra.training.jpa.api.rest.resource.ClassroomEagerResource;
import it.ra.training.jpa.api.rest.resource.ClassroomLazyResource;
import it.ra.training.jpa.entity.Classroom;
import it.ra.training.jpa.service.ClassroomService;

@Path(value = "/classroom")
@Consumes(value = "application/json")
@Produces(value = "application/json")
public class ClassroomRestApi {
	private static final Logger LOG = Logger.getLogger(ClassroomRestApi.class.getSimpleName());

	@EJB
	private ClassroomService classroomService;
	
	@GET
	public Response getAll(){
		LOG.info("getAll - START");
		final List<Classroom> theClassroomsEntity = this.classroomService.getAll();
		final List<ClassroomLazyResource> classroomsLazyResources = ClassroomEntityRestConverter.fromEntityToLazyResource(theClassroomsEntity);
		LOG.info("getAll - END");
		
		return Response.status(Status.OK).entity(classroomsLazyResources).build();
	}
	
	@GET
	@Path(value = "/eager")
	public Response getAllEager(){
		LOG.info("getAllEager - START");
		final List<Classroom> theClassroomsEntity = this.classroomService.getAllEager();
		final List<ClassroomEagerResource> classroomsEagerResources = ClassroomEntityRestConverter.fromEntityToEagerResource(theClassroomsEntity);
		LOG.info("getAllEager - END");
		
		return Response.status(Status.OK).entity(classroomsEagerResources).build();
	}
	
	@GET
	@Path(value = "/{name}")
	public Response getByName(@PathParam(value = "name") String name) {
		LOG.info(String.format("getByName - name=%s - START", name));
		final Classroom theClassroomEntity = this.classroomService.getByName(name);
		
		ClassroomLazyResource classroomLazyResource = null;
		Response responseToClient = null;
		if(theClassroomEntity == null) {
			responseToClient = Response.status(Status.NOT_FOUND).build();
		} else {
			classroomLazyResource = ClassroomEntityRestConverter.fromEntityToLazyResource(theClassroomEntity);
			responseToClient = Response.status(Status.OK).entity(classroomLazyResource).build();
		}
		LOG.info("getByName - END");
		
		return responseToClient;
	}
	
	@GET
	@Path(value = "/{name}/eager")
	public Response getByNameEager(@PathParam(value = "name") String name) {
		LOG.info(String.format("getByNameEager - name=%s - START", name));
		final Classroom theClassroomEntity = this.classroomService.getByNameEager(name);
		
		ClassroomEagerResource classroomEagerResource = null;
		Response responseToClient = null;
		if(theClassroomEntity == null) {
			responseToClient = Response.status(Status.NOT_FOUND).build();
		} else {
			classroomEagerResource = ClassroomEntityRestConverter.fromEntityToEagerResource(theClassroomEntity);
			responseToClient = Response.status(Status.OK).entity(classroomEagerResource).build();
		}
		LOG.info("getByNameEager - END");
		
		return responseToClient;
	}
	
	@POST
	@Consumes(value = "application/json")
	public Response registerNew(Classroom classroom) {
		LOG.info(String.format("registerNew - %s - START", classroom).toString());
		final boolean isNowRegistered = this.classroomService.registerNew(classroom);
		
		Response responseToClient = null;
		if(isNowRegistered) {
			responseToClient = Response.status(Response.Status.CREATED).build();
		} else {
			responseToClient = Response.status(Response.Status.NOT_MODIFIED).build();
		}
		LOG.info("registerNew - END");
		
		return responseToClient;
	}
	
	@PUT
	@Path(value = "/{id}")
	@Consumes(value = "application/json")
	public Response update(@PathParam("id") Long id, Classroom classroom) {
		LOG.info(String.format("update - id=%s, - START", classroom).toString());
		this.classroomService.update(id, classroom);
		LOG.info("update - END");
		
		return Response.status(Status.OK).build();
	}
	
	@DELETE
	@Path(value = "/{id}")
	public Response delete(@PathParam("id") Long id) {
		LOG.info(String.format("delete - id=%s - START",id).toString());
		this.classroomService.delete(id);
		LOG.info("delete - END");
		
		return Response.status(Status.NO_CONTENT).build();
	}
}
