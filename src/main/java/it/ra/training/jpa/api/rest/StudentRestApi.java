package it.ra.training.jpa.api.rest;

import java.util.List;
import java.util.logging.Logger;

import javax.ejb.EJB;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import it.ra.training.jpa.api.rest.converter.StudentEntityRestConverter;
import it.ra.training.jpa.api.rest.resource.StudentEagerResource;
import it.ra.training.jpa.api.rest.resource.StudentLazyResource;
import it.ra.training.jpa.entity.Classroom;
import it.ra.training.jpa.entity.Student;
import it.ra.training.jpa.service.StudentService;

@Path(value = "/student")
@Produces(value = "application/json")
public class StudentRestApi {
	private static final Logger LOG = Logger.getLogger(StudentRestApi.class.getSimpleName());
	
	@EJB
	private StudentService studentService;
	
	@GET
	public Response getAll(){
		LOG.info("getAll - START");
		final List<Student> theStudentsEntity = this.studentService.getAll();
		final List<StudentLazyResource> lazyResources = StudentEntityRestConverter.fromEntityToLazyResource(theStudentsEntity);
		LOG.info("getAll - END");
		
		return Response.status(Status.OK).entity(lazyResources).build();
	}
	
	@GET
	@Path(value = "/eager")
	public Response getAllEager(){
		LOG.info("getAllEager - START");
		final List<Student> theStudentsEntity = this.studentService.getAllEager();
		final List<StudentEagerResource> eagerResources = StudentEntityRestConverter.fromEntityToEagerResource(theStudentsEntity);
		LOG.info("getAllEager - END");
		
		return Response.status(Status.OK).entity(eagerResources).build();
	}
	
	@GET
	@Path(value = "/{id}/lazy")
	public Response getById(@PathParam("id") long id){
		LOG.info(String.format("getById - id=%s - START", id).toString());
		final Student theStudentEntity = this.studentService.getById(id);
		
		StudentLazyResource theLazyStudentResource = null;
		Response responseToClient = null;
		if(theStudentEntity == null) {
			responseToClient =  Response.status(Status.NOT_FOUND).build();
		}else {
			theLazyStudentResource = StudentEntityRestConverter.fromEntityToLazyResource(theStudentEntity);
			responseToClient = Response.status(Status.OK).entity(theLazyStudentResource).build();
		}
		LOG.info("getById - END");
		
		return responseToClient;
	}
	
	@GET
	@Path(value = "/{id}/eager")
	public Response getByIdEager(@PathParam("id") long id){
		LOG.info(String.format("getByIdEager - id=%s - START", id).toString());
		final Student theStudentEntity = this.studentService.getByIdEager(id);
		
		StudentEagerResource theEagerStudentResource = null;
		Response responseToClient = null;
		if(theStudentEntity == null) {
			responseToClient =  Response.status(Status.NOT_FOUND).build();
		}else {
			theEagerStudentResource = StudentEntityRestConverter.fromEntityToEagerResource(theStudentEntity);
			responseToClient = Response.status(Status.OK).entity(theEagerStudentResource).build();
		}
		LOG.info("getByIdEager - END");
		
		return responseToClient;
	}
	
	@POST
	@Consumes(value = "application/json")
	public Response registerNew(Student student) {
		LOG.info(String.format("registerNew - %s - START", student).toString());
		final boolean isNowRegistered = this.studentService.registerNew(student);
		
		Response responseToClient = null;
		if(isNowRegistered) {
			responseToClient = Response.status(Response.Status.CREATED).build();
		} else {
			responseToClient = Response.status(Response.Status.NOT_MODIFIED).build();
		}
		LOG.info("registerNew - END");
		
		return responseToClient;
	}
	
	@POST
	@Path(value = "/{code}/assignClassroom")
	@Consumes(value = "application/json")
	public Response assingClassroom(@PathParam("code") String code, final Classroom classroomToAss) {
		LOG.info(String.format("assingClassroom - code=%s, %s - START", code, classroomToAss).toString());
		this.studentService.assingClassroom(code, classroomToAss);
		LOG.info("assingClassroom - END");
		
		return Response.status(Status.OK).build();
	}
	
	@PUT
	@Path(value = "/{id}")
	@Consumes(value = "application/json")
	public Response update(@PathParam("id") Long id, Student student) {
		LOG.info(String.format("update - id=%s, - START", student).toString());
		this.studentService.update(id, student);
		LOG.info("update - END");
		
		return Response.status(Status.OK).build();
	}
	
	@DELETE
	@Path(value = "/{id}")
	public Response delete(@PathParam("id") Long id) {
		LOG.info(String.format("delete - id=%s - START",id).toString());
		this.studentService.delete(id);
		LOG.info("delete - END");
		
		return Response.status(Status.NO_CONTENT).build();
	}
}