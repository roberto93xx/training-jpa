package it.ra.training.jpa.api.rest.resource;

import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonProperty;

public class ClassroomLazyResource implements Serializable {
	private static final long serialVersionUID = 3558775901202684960L;

	@JsonProperty(value = "id")
	private Long id;
	@JsonProperty
	private String name;
	@JsonProperty(value = "busy")
	private Boolean busy;

	public ClassroomLazyResource() {
		super();
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Boolean getBusy() {
		return busy;
	}

	public void setBusy(Boolean busy) {
		this.busy = busy;
	}

}