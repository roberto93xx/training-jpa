package it.ra.training.jpa.api.rest.converter;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import it.ra.training.jpa.api.rest.resource.StudentEagerResource;
import it.ra.training.jpa.api.rest.resource.StudentLazyResource;
import it.ra.training.jpa.entity.Student;

public class StudentEntityRestConverter {

	public static StudentLazyResource fromEntityToLazyResource(Student student) {
		StudentLazyResource slr = new StudentLazyResource();
		slr.setId(student.getId());
		slr.setCode(student.getCode());
		slr.setFirstname(student.getFirstname());
		slr.setLastname(student.getLastname());
		slr.setGender(student.getGender());
		slr.setBornDate(student.getBornDate());
		
		return slr;
	}
	
	public static List<StudentLazyResource> fromEntityToLazyResource(List<Student> students){
		if(students == null || students.isEmpty()) {
			return new ArrayList<StudentLazyResource>();
		}
		
		List<StudentLazyResource> lazyResources = students.stream()
				.map( StudentEntityRestConverter::fromEntityToLazyResource )
				.collect(Collectors.toList());
		
		return lazyResources;
	}
	
	public static StudentEagerResource fromEntityToEagerResource(Student student) {
		StudentEagerResource ser = new StudentEagerResource();
		ser.setId(student.getId());
		ser.setCode(student.getCode());
		ser.setFirstname(student.getFirstname());
		ser.setLastname(student.getLastname());
		ser.setGender(student.getGender());
		ser.setBornDate(student.getBornDate());
		ser.setClassrooms(student.getClassrooms());
		
		return ser;
	}
	
	public static List<StudentEagerResource> fromEntityToEagerResource(List<Student> students){
		if(students == null || students.isEmpty()) {
			return new ArrayList<StudentEagerResource>();
		}
		
		List<StudentEagerResource> eagerResources = students.stream()
				.map( StudentEntityRestConverter::fromEntityToEagerResource )
				.collect(Collectors.toList());
		
		return eagerResources;
	}
}