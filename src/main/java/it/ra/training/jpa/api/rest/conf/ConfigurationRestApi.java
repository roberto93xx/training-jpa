package it.ra.training.jpa.api.rest.conf;

import javax.ws.rs.ApplicationPath;
import javax.ws.rs.core.Application;

@ApplicationPath(value = "/rest/api/v1")
public class ConfigurationRestApi extends Application { }
