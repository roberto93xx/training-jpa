package it.ra.training.jpa.api.rest.resource;

import java.io.Serializable;
import java.util.Date;

import javax.json.bind.annotation.JsonbProperty;

import it.ra.training.jpa.entity.util.Gender;

public class StudentLazyResource implements Serializable {
	private static final long serialVersionUID = 3558775901202684960L;

	@JsonbProperty(value = "id")
	private Long id;
	@JsonbProperty(value = "code")
	private String code;
	@JsonbProperty(value = "firstName")
	private String firstname;
	@JsonbProperty(value = "last")
	private String lastname;
	@JsonbProperty(value = "gender")
	private Gender gender;
	@JsonbProperty(value = "borndate")
	private Date bornDate;

	public StudentLazyResource() {
		super();
	}
	
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getFirstname() {
		return firstname;
	}

	public void setFirstname(String firstname) {
		this.firstname = firstname;
	}

	public String getLastname() {
		return lastname;
	}

	public void setLastname(String lastname) {
		this.lastname = lastname;
	}

	public Gender getGender() {
		return gender;
	}

	public void setGender(Gender gender) {
		this.gender = gender;
	}

	public Date getBornDate() {
		return bornDate;
	}

	public void setBornDate(Date bornDate) {
		this.bornDate = bornDate;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("StudentLazyResource [id=");
		builder.append(id);
		builder.append(", code=");
		builder.append(code);
		builder.append(", firstname=");
		builder.append(firstname);
		builder.append(", lastname=");
		builder.append(lastname);
		builder.append(", gender=");
		builder.append(gender);
		builder.append(", bornDate=");
		builder.append(bornDate);
		builder.append("]");
		return builder.toString();
	}
}