package it.ra.training.jpa.api.rest.converter;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import it.ra.training.jpa.api.rest.resource.ClassroomEagerResource;
import it.ra.training.jpa.api.rest.resource.ClassroomLazyResource;
import it.ra.training.jpa.entity.Classroom;

public class ClassroomEntityRestConverter {

	public static ClassroomLazyResource fromEntityToLazyResource(Classroom classroom) {
		if(classroom == null) {
			return null;
		}
		ClassroomLazyResource clr = new ClassroomLazyResource();
		clr.setId(classroom.getId());
		clr.setName(classroom.getName());
		clr.setBusy(classroom.getBusy());
		
		return clr;
	}
	
	public static List<ClassroomLazyResource> fromEntityToLazyResource(List<Classroom> classrooms){
		if(classrooms == null || classrooms.isEmpty()) {
			return new ArrayList<ClassroomLazyResource>();
		}
		
		List<ClassroomLazyResource> lazyResources = classrooms.stream()
				.map( ClassroomEntityRestConverter::fromEntityToLazyResource )
				.collect(Collectors.toList());
		
		return lazyResources;
	}
	
	public static ClassroomEagerResource fromEntityToEagerResource(Classroom classroom) {
		if(classroom == null) {
			return null;
		}
		ClassroomEagerResource cer = new ClassroomEagerResource();
		cer.setId(classroom.getId());
		cer.setName(classroom.getName());
		cer.setBusy(classroom.getBusy());
		cer.setStudents(classroom.getStudents());
		
		return cer;
	}
	
	public static List<ClassroomEagerResource> fromEntityToEagerResource(List<Classroom> classoroms){
		if(classoroms == null || classoroms.isEmpty()) {
			return new ArrayList<ClassroomEagerResource>();
		}
		
		List<ClassroomEagerResource> eagerResources = classoroms.stream()
				.map( ClassroomEntityRestConverter::fromEntityToEagerResource )
				.collect(Collectors.toList());
		
		return eagerResources;
	}
}