package it.ra.training.jpa.api.rest.resource;

import java.util.HashSet;
import java.util.Set;

import com.fasterxml.jackson.annotation.JsonProperty;

import it.ra.training.jpa.entity.Classroom;

public class StudentEagerResource extends StudentLazyResource {
	private static final long serialVersionUID = -1431522876746189434L;
	
	@JsonProperty(value = "classrooms")
	private Set<Classroom> classrooms = new HashSet<>();

	public Set<Classroom> getClassrooms() {
		return classrooms;
	}

	public void setClassrooms(Set<Classroom> classrooms) {
		this.classrooms = classrooms;
	}
}