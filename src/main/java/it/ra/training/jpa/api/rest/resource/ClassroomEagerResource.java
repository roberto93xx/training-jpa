package it.ra.training.jpa.api.rest.resource;

import java.util.HashSet;
import java.util.Set;

import com.fasterxml.jackson.annotation.JsonProperty;

import it.ra.training.jpa.entity.Student;

public class ClassroomEagerResource extends ClassroomLazyResource {
	private static final long serialVersionUID = 3558775901202684960L;

	@JsonProperty(value = "students")
	private Set<Student> students = new HashSet<>();

	public ClassroomEagerResource() {
		super();
	}

	public Set<Student> getStudents() {
		return students;
	}

	public void setStudents(Set<Student> students) {
		this.students = students;
	}

}