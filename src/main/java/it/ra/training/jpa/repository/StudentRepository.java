package it.ra.training.jpa.repository;

import java.util.List;

import it.ra.training.jpa.entity.Student;

public interface StudentRepository {
	public List<Student> getAll();

	public List<Student> getAllEager();
	
	public Student getById(Long id);

	public Student getByIdEager(Long id);
	
	// code <==> matricola
	public Student getByCode(String code);

	public List<Student> getByFirstname(String firstname);

	public List<Student> getByLastname(String lastname);
	
	public List<Student> getByFirstnameAndLastname(String firstname, String lastname);

	public boolean save(Student student);
	
	public Student update(Student student);
	
	public void delete(Student student);

}