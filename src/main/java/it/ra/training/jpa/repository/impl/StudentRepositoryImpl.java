package it.ra.training.jpa.repository.impl;

import java.util.Collections;
import java.util.List;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;

import it.ra.training.jpa.entity.Student;
import it.ra.training.jpa.repository.StudentRepository;

@Stateless
public class StudentRepositoryImpl implements StudentRepository {

	@PersistenceContext(name = "MyPU")
	private EntityManager entityManager;

	public List<Student> getAll() {
		TypedQuery<Student> students = this.entityManager.createNamedQuery("getAll", Student.class);
		if (students != null) {
			return students.getResultList();
		}
		return Collections.emptyList();
	}
	
	public List<Student> getAllEager() {
		TypedQuery<Student> students = this.entityManager.createNamedQuery("getAllEager", Student.class);
		if (students != null) {
			return students.getResultList();
		}
		return Collections.emptyList();
	}

	public Student getById(Long id) {
		TypedQuery<Student> student = this.entityManager.createNamedQuery("getById", Student.class);
		student.setParameter("theId", id);
		List<Student> students = student.getResultList();
		return students == null || students.isEmpty() ? null : students.get(0);
	}
	
	public Student getByIdEager(Long id) {
		TypedQuery<Student> student = this.entityManager.createNamedQuery("getByIdEager", Student.class);
		student.setParameter("theId", id);
		List<Student> students = student.getResultList();
		return students == null || students.isEmpty() ? null : students.get(0);
	}
	
	public Student getByCode(String code) {
		TypedQuery<Student> student = this.entityManager.createNamedQuery("getByCode", Student.class);
		student.setParameter("theCode", code);
		List<Student> students = student.getResultList();
		return students == null || students.isEmpty() ? null : students.get(0);
	}

	public List<Student> getByFirstname(String firstname) {
		TypedQuery<Student> students = this.entityManager.createNamedQuery("getByFirstname", Student.class);
		students.setParameter("theFirstname", firstname);
		return students.getResultList();
	}

	public List<Student> getByLastname(String lastname) {
		TypedQuery<Student> students = this.entityManager.createNamedQuery("getByLastname", Student.class);
		students.setParameter("theLastname", lastname);
		return students.getResultList();
	}
	
	public List<Student> getByFirstnameAndLastname(String firstname, String lastname) {
		TypedQuery<Student> students = this.entityManager.createNamedQuery("getByFirstNameAndLastname", Student.class);
		students.setParameter("theFirstname", firstname);
		students.setParameter("theLastname", lastname);
		return students.getResultList();
	}

	public List<Student> getByGender(String gender) {
		TypedQuery<Student> students = this.entityManager.createNamedQuery("getByGender", Student.class);
		students.setParameter("theGender", gender);
		return students.getResultList();
	}

	public boolean save(Student student) {
		this.entityManager.persist(student);
		return this.entityManager.contains(student);
	}

	public Student update(Student student) {
		return this.entityManager.merge(student);
	}

	public void delete(Student student) {
		if (student != null) {
			this.entityManager.remove(student);
		}
	}
}