package it.ra.training.jpa.repository;

import java.util.List;

import it.ra.training.jpa.entity.Classroom;

public interface ClassroomRepository {

	public List<Classroom> getAll();

	public List<Classroom> getAllEager();
	
	public Classroom getById(Long id);

	public Classroom getByName(String name);

	public Classroom getByNameEager(String name);
	
	public List<Classroom> getByBusy(boolean busy);
	
	public boolean save(Classroom classroomToSv);
	
	public Classroom update(Classroom classroomToUpd);
	
	public void delete(Classroom classroomToDel);
}
