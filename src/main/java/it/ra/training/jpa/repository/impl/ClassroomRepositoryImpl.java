package it.ra.training.jpa.repository.impl;

import java.util.List;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;

import it.ra.training.jpa.entity.Classroom;
import it.ra.training.jpa.repository.ClassroomRepository;

@Stateless
public class ClassroomRepositoryImpl implements ClassroomRepository {

	@PersistenceContext(name = "MyPU")
	private EntityManager entityManager;
	
	@Override
	public List<Classroom> getAll() {
		TypedQuery<Classroom> query = this.entityManager.createQuery("SELECT c FROM Classroom c", Classroom.class);
		return query.getResultList();
	}
	
	@Override
	public List<Classroom> getAllEager() {
		TypedQuery<Classroom> query = this.entityManager.createQuery("SELECT c FROM Classroom c LEFT JOIN FETCH c.students ", Classroom.class);
		return query.getResultList();
	}

	@Override
	public Classroom getById(Long id) {
		TypedQuery<Classroom> query = this.entityManager.createQuery("SELECT c FROM Classroom c WHERE c.id = ?1", Classroom.class);
		query.setParameter(1, id);
		List<Classroom> classrooms = query.getResultList();
		return classrooms == null || classrooms.isEmpty() ? null : classrooms.get(0);
	}
	
	@Override
	public Classroom getByNameEager(String name) {
		TypedQuery<Classroom> query = this.entityManager.createQuery("SELECT c FROM Classroom c LEFT JOIN FETCH c.students WHERE c.name = ?1", Classroom.class);
		query.setParameter(1, name);
		List<Classroom> classrooms = query.getResultList();
		return classrooms == null || classrooms.isEmpty() ? null : classrooms.get(0);
	}

	@Override
	public Classroom getByName(String name) {
		TypedQuery<Classroom> query = this.entityManager.createQuery("SELECT c FROM Classroom c WHERE c.name = ?1", Classroom.class);
		query.setParameter(1, name);
		List<Classroom> classrooms = query.getResultList();
		return classrooms == null || classrooms.isEmpty() ? null : classrooms.get(0);
	}

	@Override
	public List<Classroom> getByBusy(boolean busy) {
		TypedQuery<Classroom> query = this.entityManager.createQuery("SELECT c FROM Classroom c WHERE c.busy = ?1", Classroom.class);
		query.setParameter(1, busy);
		return query.getResultList();
	}

	@Override
	public boolean save(Classroom classroomToSv) {
		this.entityManager.persist(classroomToSv);
		
		// verify if after to persist entity, classroom to persist is in persistence context.
		// if is in, then the entity is effectively saved, otherwise none.
		return this.entityManager.contains(classroomToSv);
	}

	@Override
	public Classroom update(Classroom classroomToUpd) {
		return this.entityManager.merge(classroomToUpd);
	}

	@Override
	public void delete(Classroom classroomToDel) {
		this.entityManager.remove(classroomToDel);
	}
}