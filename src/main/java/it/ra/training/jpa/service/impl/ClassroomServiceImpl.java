package it.ra.training.jpa.service.impl;

import java.util.List;
import java.util.logging.Logger;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.transaction.Transactional;
import javax.transaction.Transactional.TxType;

import it.ra.training.jpa.entity.Classroom;
import it.ra.training.jpa.repository.ClassroomRepository;
import it.ra.training.jpa.service.ClassroomService;

@Stateless
public class ClassroomServiceImpl implements ClassroomService {
	private static final Logger LOG = Logger.getLogger(StudentServiceImpl.class.getSimpleName());

	@EJB
	private ClassroomRepository classroomRepo;

	@Override
	public List<Classroom> getAll() {
		return this.classroomRepo.getAll();
	}
	
	@Override
	public List<Classroom> getAllEager() {
		return this.classroomRepo.getAllEager();
	}

	@Override
	public Classroom getById(Long id) {
		if (id == null) {
			return null;
		}
		Classroom theClassroom = this.classroomRepo.getById(id);
		if(theClassroom != null) {
			LOG.info(String.format("theClassroom.students=", theClassroom.getStudents()));
		}
		return theClassroom;
	}
	
	@Override
	public Classroom getByName(String name) {
		if(name == null || name.isEmpty()) {
			return null;
		}
		return this.classroomRepo.getByName(name);
	}
	
	@Override
	public Classroom getByNameEager(String name) {
		if(name == null || name.isEmpty()) {
			return null;
		}
		return this.classroomRepo.getByNameEager(name);
	}

	@Override
	public List<Classroom> getByBusy(boolean busy) {
		return this.classroomRepo.getByBusy(busy);
	}


	@Override
	@Transactional(value = TxType.REQUIRED)
	public boolean registerNew(Classroom classroom) {
		if(!alreadyExistsClassroom(classroom.getName())) {
			return this.save(classroom);
		}
		return false;	
	}
	
	public boolean alreadyExistsClassroom(String name) {
		return this.getByName(name) != null;
	}

	@Transactional(value = TxType.REQUIRED)
	public boolean save(Classroom classroomToSv) {
		if(classroomToSv == null) {
			return false;
		}
		return this.classroomRepo.save(classroomToSv);
	}

	@Override
	@Transactional(value = TxType.REQUIRED)
	public void update(Long idClassroomToUpd, Classroom classroomToUpd) {
		if(idClassroomToUpd == null || classroomToUpd == null) {
			return ;
		}
		
		Classroom theClassroomFetch = this.getById(idClassroomToUpd);
		if(theClassroomFetch == null) {
			return ;
		}
		
		classroomToUpd.setId(idClassroomToUpd);
		this.classroomRepo.update(classroomToUpd);

	}

	@Override
	@Transactional(value = TxType.REQUIRED)
	public void delete(Long idClassroomToDel) {
		Classroom theClassroomToDel = this.getById(idClassroomToDel);
		
		if(theClassroomToDel == null) {
			return ;
		}
		
		this.classroomRepo.delete(theClassroomToDel);
	}
}