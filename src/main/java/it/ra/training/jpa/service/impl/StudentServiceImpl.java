package it.ra.training.jpa.service.impl;

import java.util.List;
import java.util.logging.Logger;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.transaction.Transactional;
import javax.transaction.Transactional.TxType;

import it.ra.training.jpa.entity.Classroom;
import it.ra.training.jpa.entity.Student;
import it.ra.training.jpa.repository.StudentRepository;
import it.ra.training.jpa.service.ClassroomService;
import it.ra.training.jpa.service.StudentService;

@Stateless
public class StudentServiceImpl implements StudentService {
	private static final Logger LOG = Logger.getLogger(StudentServiceImpl.class.getSimpleName());

	@PersistenceContext(name = "MyPU")
	private EntityManager entityManager;

	@EJB
	private ClassroomService classroomService;
	@EJB
	private StudentRepository studentRepo;

	@Override
	public List<Student> getAll() {
		return this.studentRepo.getAll();
	}
	
	@Override
	public List<Student> getAllEager() {
		return this.studentRepo.getAllEager();
	}

	@Override
	public Student getById(Long id) {
		if (id == null) {
			return null;
		}
		Student theStudent = this.studentRepo.getById(id);
		if(theStudent != null) {
			LOG.info(String.format("theStudent.classrooms=", theStudent.getClassrooms()));
		}
		return theStudent;
	}
	
	@Override
	public Student getByIdEager(Long id) {
		if (id == null) {
			return null;
		}
		Student theStudent = this.studentRepo.getByIdEager(id);
		if(theStudent != null) {
			LOG.info(String.format("theStudent.classrooms=", theStudent.getClassrooms()));
		}
		return theStudent;
	}

	@Override
	public Student getByCode(String code) {
		if (code == null || code.isEmpty()) {
			return null;
		}
		return this.studentRepo.getByCode(code);
	}

	@Override
	public List<Student> getByFirstnameAndLastname(String firstname, String lastname) {
		if ((firstname == null && lastname == null) || (firstname.isEmpty() && lastname.isEmpty())) {
			return null;
		}
		return this.studentRepo.getByFirstnameAndLastname(firstname, lastname);
	}

	@Override
	public List<Student> getByFirstname(String firstname) {
		if (firstname == null || firstname.isEmpty()) {
			return null;
		}
		return this.studentRepo.getByFirstname(firstname);
	}

	@Override
	public List<Student> getByLastname(String lastname) {
		if (lastname == null || lastname.isEmpty()) {
			return null;
		}
		return this.studentRepo.getByLastname(lastname);
	}

	@Override
	@Transactional(value = TxType.REQUIRED)
	public boolean registerNew(Student student) {
		if (!alreadyExistsStudent(student.getCode())) {
			return this.save(student);
		}
		return false;
	}

	public boolean alreadyExistsStudent(String code) {
		return this.getByCode(code) != null;
	}

	@Transactional(value = TxType.REQUIRED)
	public boolean save(Student student) {
		if (student == null) {
			return false;
		}
		return this.studentRepo.save(student);
	}

	@Override
	@Transactional(value = TxType.REQUIRED)
	public void update(Long idStudentToUpd, Student studentToUpd) {
		if (idStudentToUpd == null || studentToUpd == null) {
			return;
		}

		Student theStudentFetch = this.getById(idStudentToUpd);
		if (theStudentFetch == null) {
			return;
		}

		studentToUpd.setId(idStudentToUpd);
		this.studentRepo.update(studentToUpd);
	}

	@Override
	@Transactional(value = TxType.REQUIRED)
	public void delete(Long idStudentToDel) {
		Student theStudentToDel = this.getById(idStudentToDel);

		if (theStudentToDel == null) {
			return;
		}

		this.studentRepo.delete(theStudentToDel);
	}

	@Transactional(value = TxType.REQUIRED)
	public void assingClassroom(String code, Classroom classrooom) {
		if (classrooom == null) {
			return;
		}

		final Student theStudentFetch = this.getByCode(code);
		final Classroom theClassroomFetch = this.classroomService.getByName(classrooom.getName());
		if (theStudentFetch == null || theClassroomFetch == null) {
			return;
		}

		theStudentFetch.getClassrooms().add(theClassroomFetch);
		this.update(theStudentFetch.getId(), theStudentFetch);
		theClassroomFetch.getStudents().add(theStudentFetch);
		this.classroomService.update(theClassroomFetch.getId(), theClassroomFetch);
	}
}