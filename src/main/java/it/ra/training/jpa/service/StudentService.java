package it.ra.training.jpa.service;

import java.util.List;

import it.ra.training.jpa.entity.Classroom;
import it.ra.training.jpa.entity.Student;

public interface StudentService {

	public List<Student> getAll();

	public List<Student> getAllEager();
	
	public Student getById(Long id);
	
	public Student getByIdEager(Long id);
	
	public Student getByCode(String code);
	
	public List<Student> getByFirstname(String firstname);

	public List<Student> getByLastname(String lastname);

	public List<Student> getByFirstnameAndLastname(String firstname, String lastname);
	
	public boolean registerNew(Student student);

	public void update(Long idStudentToUpd, Student student);
	
	public void delete(Long idStudentToDel);
	
	public void assingClassroom(String code, Classroom classroomToAss);
}