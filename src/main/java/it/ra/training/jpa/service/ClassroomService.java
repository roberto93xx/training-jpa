package it.ra.training.jpa.service;

import java.util.List;

import it.ra.training.jpa.entity.Classroom;

public interface ClassroomService {

	public List<Classroom> getAll();

	public List<Classroom> getAllEager();
	
	public Classroom getById(Long id);

	public Classroom getByName(String name);
	
	public Classroom getByNameEager(String name);
	
	public List<Classroom> getByBusy(boolean busy);
	
	public boolean save(Classroom classroomToSv);

	public boolean registerNew(Classroom classroom);

	public void update(Long idClassroomToUpd, Classroom classroom);
	
	public void delete(Long idClassroomToDel);

}
