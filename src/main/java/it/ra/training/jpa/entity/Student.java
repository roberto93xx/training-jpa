package it.ra.training.jpa.entity;

import java.io.Serializable;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import com.fasterxml.jackson.annotation.JsonIgnore;

import it.ra.training.jpa.entity.util.Gender;

@Entity
@Table(name = "STUDENT")
@NamedQueries(value = { 
		@NamedQuery(name = "getAll", 					query = "SELECT s FROM Student s"),
		@NamedQuery(name = "getAllEager", 				query = "SELECT s FROM Student s LEFT JOIN FETCH s.classrooms"),
		@NamedQuery(name = "getById", 		 			query = "SELECT s FROM Student s WHERE s.id =:theId"),
		@NamedQuery(name = "getByIdEager", 		 		query = "SELECT s FROM Student s LEFT JOIN FETCH s.classrooms WHERE s.id =:theId"),
		@NamedQuery(name = "getByCode",		 			query = "SELECT s FROM Student s WHERE s.code =:theCode"),
		@NamedQuery(name = "getByFirstname", 			query = "SELECT s FROM Student s WHERE s.firstname =:theFname"),
		@NamedQuery(name = "getByLastname",  			query = "SELECT s FROM Student s WHERE s.lastname =:theLname"),
		@NamedQuery(name = "getByFirstnameAndLastname", query = "SELECT s FROM Student s WHERE s.firstname =:theFname AND s.lastname =:theLname"),
		@NamedQuery(name = "getByGender", 				query = "SELECT s FROM Student s WHERE s.gender =:theGender"), 
})
public class Student implements Serializable {
	private static final long serialVersionUID = 4392965437034434775L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "ID")
	private Long id;
	@Column(name = "CODE") // matricola
	@NotNull(message = "code not be null")
	@Size(min = 6, max = 6, message = "code must have 6 char")
	private String code;
	@Column(name = "FIRSTNAME")
	@NotNull(message = "firstname cannot be null")
	private String firstname;
	@Column(name = "LASTNAME")
	@NotNull(message = "lastname cannot be null")
	private String lastname;
	@Column(name = "GENDER")
	@Enumerated(value = EnumType.STRING)
	private Gender gender;
	@Column(name = "BORN_DATE")
	@Temporal(TemporalType.DATE)
	private Date bornDate;
	@JsonIgnore
	@ManyToMany(fetch = FetchType.LAZY)
	@JoinTable(name = "STUDENT_X_CLASSROOM", joinColumns = @JoinColumn(name = "ID_STUDENT"), inverseJoinColumns = @JoinColumn(name = "ID_CLASSROOM"))
	private Set<Classroom> classrooms;

	public Student() {
		super();
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getFirstname() {
		return firstname;
	}

	public void setFirstname(String firstname) {
		this.firstname = firstname;
	}

	public String getLastname() {
		return lastname;
	}

	public void setLastname(String lastname) {
		this.lastname = lastname;
	}

	public Gender getGender() {
		return gender;
	}

	public void setGender(Gender gender) {
		this.gender = gender;
	}

	public Date getBornDate() {
		return bornDate;
	}

	public void setBornDate(Date bornDate) {
		this.bornDate = bornDate;
	}

	public Set<Classroom> getClassrooms() {
		if(this.classrooms == null) {
			this.classrooms = new HashSet<>();
		}
		return classrooms;
	}

	public void setClassrooms(Set<Classroom> classrooms) {
		this.classrooms = classrooms;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((code == null) ? 0 : code.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Student other = (Student) obj;
		if (code == null) {
			if (other.code != null)
				return false;
		} else if (!code.equals(other.code))
			return false;
		return true;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("Student [id=");
		builder.append(id);
		builder.append(", code=");
		builder.append(code);
		builder.append(", firstname=");
		builder.append(firstname);
		builder.append(", lastname=");
		builder.append(lastname);
		builder.append(", gender=");
		builder.append(gender);
		builder.append(", bornDate=");
		builder.append(bornDate);
		builder.append(", classrooms=");
		builder.append(classrooms);
		builder.append("]");
		return builder.toString();
	}
}