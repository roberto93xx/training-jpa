package it.ra.training.jpa.entity.util;

public enum Gender {
	M("MALE"),
	F("FEMALE"),
	T("TRANSGENDER");
	
	private String value;
	
	private Gender(String value) {
		this.value = value;
	}

	public void setValue(String value) {
		this.value = value;
	}

	public String getValue() {
		return value;
	}
	
	public Gender getGenderByValue(String genderValue) {
		return Gender.valueOf(genderValue);
	}
	
	public Gender[] getGenderValues() {
		return Gender.values();
	}
}